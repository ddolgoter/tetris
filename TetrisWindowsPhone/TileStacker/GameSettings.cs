﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TetrisWindowsPhone
{
    public class GameSettings
    {
        public const int START_FALL_TIME = 800;
        public const int FAST_FALL_TIME = 40;

        public int FallTime { get; set; }
        public int FastFallTime { get; set; }
        public int GameScore { get; set; }

        public bool IsProgressive { get; private set; }

        public int PlayLevel
        {
            get;
            private set;
        }

        private int _level;
        public int Level
        {
            get
            {
                return _level;
            }
            private set
            {
                if (value <= 15)
                    _level = value;
            }
        }

        public int MaxLevel { get; set; }
        public int LinesNeededToLevelUp { get; set; }
        public int LevelUpSpeedIncrease { get; set; }
        public int LinesCleared { get; set; }
        public int LinesSinceLevelUp { get; set; }

        public GameSettings()
        {
            Reset();
        }

        /// <summary>
        /// Resets all state for a new game
        /// </summary>
        public void Reset()
        {
            FastFallTime = 40;
            LinesNeededToLevelUp = 10;
            LevelUpSpeedIncrease = 44;
            Level = 1;
            MaxLevel = 15;
            GameScore = 0;
            FallTime = START_FALL_TIME;
            LinesSinceLevelUp = 0;
            LinesCleared = 0;
        }

        public void SetPlayLevel(int level)
        {
            SetPlayLevelDoNotChangeProgressive(level);

            if (level > 0)
                IsProgressive = false;
        }

        /// <summary>
        /// Us used when loading a saved game
        /// </summary>
        /// <param name="level"></param>
        public void SetPlayLevelDoNotChangeProgressive(int level)
        {
            FallTime = START_FALL_TIME - ((level - 1) * LevelUpSpeedIncrease);
            Level = level;
        }

        public void SetProgressive()
        {
            IsProgressive = true;
            PlayLevel = 0;
        }

        public void UpdateFromFallenShape(int addLines)
        {
            GameScore++;
            LinesCleared += addLines;
            GameScore += addLines * 10;

            LinesSinceLevelUp += addLines;
            if (IsProgressive && LinesSinceLevelUp >= LinesNeededToLevelUp && Level < MaxLevel)
            {
                LinesSinceLevelUp -= LinesNeededToLevelUp;
                FallTime -= LevelUpSpeedIncrease;
                Level++;
            }
        }

    }
}
