using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TetrisWindowsPhone
{
    public class GameMenuPixelData
    {
        Texture2D _texture;
        int [,] _pixelMap;

        public GameMenuPixelData(Texture2D texture)
        {
            _texture = texture;
            CreatePixelMap();
        }


        private void CreatePixelMap()
        {
            _pixelMap = new int [_texture.Bounds.Height, _texture.Bounds.Width];
            var retrievedColors = new Color[_texture.Bounds.Height * _texture.Bounds.Width];

            _texture.GetData<Color>(
                0,
                _texture.Bounds,
                retrievedColors,
                0,
                _texture.Bounds.Height * _texture.Bounds.Width);

            for (int y = 0; y < _pixelMap.GetLength(0); y++)
                for (int x = 0; x < _pixelMap.GetLength(1); x++)
                {
                    var color = retrievedColors[y * _pixelMap.GetLength(1) + x];
                    if (color == Color.Black)
                        _pixelMap[y, x] = 1;

                    if (color == Color.Red)
                        _pixelMap[y, x] = 2;
                }
        }

        public int GetPixel(int X, int Y)
        {
            return _pixelMap[Y, X];
        }

        public int Height
        {
            get
            {
                return _pixelMap.GetLength(0);
            }
        }

        public int Width
        {
            get
            {
                return _pixelMap.GetLength(1);
            }
        }
    }
}
