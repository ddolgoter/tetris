﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TetrisWindowsPhone
{
    public enum TextureRotateOptions
    {
        None, Left, Right, Flip
    }

    public class ButtonComponent : DrawableGameComponent
    {
        private string _buttonTextureResource, _buttonPressedTextureResource;
        private Rectangle _buttonRectange;
        private Texture2D _buttonTexture, _buttonPressedTexture;
        SpriteBatch _spriteBatch;

        private bool _updated;

        public ButtonComponent(Game game, string buttonTextureResource, string buttonPressedTextureResource, Rectangle buttonRectangle)
            : base(game)
        {
            _buttonTextureResource = buttonTextureResource;
            _buttonPressedTextureResource = buttonPressedTextureResource;
            _buttonRectange = buttonRectangle;
        }

        public override void Initialize()
        {
            _spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _buttonTexture = Game.Content.Load<Texture2D>(_buttonTextureResource);
            _buttonPressedTexture = Game.Content.Load<Texture2D>(_buttonPressedTextureResource);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            //TODO: not a generic component if needed to be converted;
            var game = (Game1)this.Game;
            if ((DisplayOnGameStates & game.GameState) == 0)
                return;

            if (_updated)
            {
                _updated = false;
                return;
            }

            var mouse = Mouse.GetState();

            if (Utils.RectangleClicked(_buttonRectange, mouse) && !MousePressed)
            {
                MousePressed = true;
                MouseHeldMilliseconds = 0;
            }
            else if (Utils.RectangleClicked(_buttonRectange, mouse) && MousePressed)
            {
                MouseHeldMilliseconds += gameTime.ElapsedGameTime.Milliseconds;
            }
            else
            {
                MousePressed = false;
                MouseHeldMilliseconds = 0;
            }


            _updated = true;
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            //TODO: not a generic component if needed to be converted;
            var game = (Game1)this.Game;
            if ((DisplayOnGameStates & game.GameState) == 0)
                return;

            var position = new Vector2(_buttonRectange.X, _buttonRectange.Y);
            var texture = MousePressed ? _buttonPressedTexture : _buttonTexture;
            var scale = new Vector2(_buttonRectange.Width / (float)texture.Width, _buttonRectange.Height / (float)texture.Height);

            _spriteBatch.Begin();
            _spriteBatch.Draw(
                texture,
                position: position,
                sourceRectangle: null,
                color: Color.White,
                rotation: Rotate,
                origin: Origin,
                scale: scale,
                effects: SpriteEffects.None,
                layerDepth: 0);

            _spriteBatch.End();

            base.Draw(gameTime);
        }


        /// <summary>
        /// If the left mouse button is pressed over a button and is pressed since some update in the past will display time in
        /// milliseconds since this button was pressed, otherwise returns zero
        /// </summary>
        public int MouseHeldMilliseconds
        {
            get;
            private set;
        }

        /// <summary>
        /// Shows if mouse is pressed over the button
        /// </summary>
        public bool MousePressed
        {
            get;
            private set;
        }

        public bool MouseJustPressed
        {
            get { return MousePressed && MouseHeldMilliseconds == 0; }
        }


        public Vector2 Origin { get; set; }
        public float Rotate { get; set; }

        public GameState DisplayOnGameStates { get; set; }
    }
}
