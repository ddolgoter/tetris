using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Advertising.Mobile.Xna;
using System.Device.Location;

namespace TetrisWindowsPhone
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        
#if DEBUG
        public static readonly string ApplicationId = "test_client";
        public static readonly string AdUnitId = "Image480_80";//other test values: Image480_80, Image300_50, TextAd
#else
        public static readonly string ApplicationId = "c29b0956-6a1b-4a9f-8dcc-b748cbf28a12";
        public static readonly string AdUnitId = "10728167"; 
#endif
        public DrawableAd bannerAd;
        // We will use this to find the device location for better ad targeting.
        public GeoCoordinateWatcher gcw = null;
        public Advertising advertising;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        ButtonComponent buttonLeft, buttonRight, buttonDown, buttonRotate;
        Texture2D tileTex, boardTex, filledTileTex, emptyTileTex, gameOverTex, shapeTrajectoryTex;
        SpriteFont generalFont;
        Rectangle boardRect, tileRect, shapeQueueRect;

        TetrisLogic tetrisLogic;
        Queue<Shape> shapesQueue;
        ShapeGenerator shapeGenerator;

        int timeSiceRightPressed, timeSinceLeftPressed;

        public const int SHAPES_IN_QUEUE = 4;
        static int TILE_DIM = 24;

        int sinceLastFall = 0;
        GameSettings gameSettings;
        GameBoard gameBoard;
        public bool GameInProgress { get; set; }

        /// <summary>
        /// Don't want to exit the program when clicking back for a game in progress
        /// </summary>
        public bool PressedBackFromGameInProgress { get; set; }

        public GameState GameState;

        public GameMenu GameMenu;

        public GraphicsDeviceManager Graphics
        {
            get
            {
                return graphics;
            }
        }

        public Game1()
        {

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            //TODO: Windows Centric
            Window.AllowUserResizing = false;
            graphics.SupportedOrientations = DisplayOrientation.Portrait;
            var h = graphics.PreferredBackBufferHeight;
            var w = graphics.PreferredBackBufferWidth;
            graphics.PreferredBackBufferWidth = h;
            graphics.PreferredBackBufferHeight = w;

        }

        private void SetSizesForResolution()
        {
            var boardWidth = (int)(graphics.PreferredBackBufferWidth * 0.5);
            TILE_DIM = boardWidth / 10;
            var boardLeftMargin = (int)(graphics.PreferredBackBufferWidth * 0.25);
            // TODO: Windows centric
            IsMouseVisible = true;
            boardRect = new Rectangle(boardLeftMargin, 0, TILE_DIM * 10, TILE_DIM * 20);
            tileRect = new Rectangle(0, 0, (int)(TILE_DIM * 0.8), (int)(TILE_DIM * 0.8));

            shapeQueueRect = new Rectangle(boardLeftMargin + boardWidth + TILE_DIM, 0, 3 * TILE_DIM, 10 * TILE_DIM);


            var buttonSize = (int)(TILE_DIM * 4.5);

            var validForButtons = GameState.Playing | GameState.GameOver;
            buttonLeft = new ButtonComponent(this, @"Images/down_arrow", @"Images/down_arrow_pressed",
                new Rectangle(boardLeftMargin, TILE_DIM * 21, buttonSize, buttonSize))
            {
                Origin = new Vector2(0, 100),
                Rotate = 1.57079633f, // rotate 90 degrees
                DisplayOnGameStates = validForButtons
            };

            buttonRight = new ButtonComponent(this, @"Images/down_arrow", @"Images/down_arrow_pressed",
                new Rectangle(boardLeftMargin + buttonSize + TILE_DIM, TILE_DIM * 21, buttonSize, buttonSize))
            {
                Origin = new Vector2(100, 0),
                Rotate = -1.57079633f, // rotate 90 degrees
                DisplayOnGameStates = validForButtons
            };

            buttonDown = new ButtonComponent(this, @"Images/down_arrow", @"Images/down_arrow_pressed",
                new Rectangle(boardLeftMargin + buttonSize + TILE_DIM, TILE_DIM * 22 + buttonSize, buttonSize, buttonSize)) { DisplayOnGameStates = validForButtons };

            buttonRotate = new ButtonComponent(this, @"Images/rotate", @"Images/rotate_pressed",
                new Rectangle(boardLeftMargin, TILE_DIM * 22 + buttonSize, buttonSize, buttonSize)) { DisplayOnGameStates = validForButtons };
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            SetSizesForResolution();

            Components.Add(buttonLeft);
            Components.Add(buttonRight);
            Components.Add(buttonDown);
            Components.Add(buttonRotate);

            GameMenu = new GameMenu(this);
            Components.Add(GameMenu);

            GameState = GameState.StartMenu;
            gameSettings = new GameSettings();


            // Initialize the AdGameComponent with your ApplicationId and add it to the game.
            advertising = new Advertising(this, graphics);
            AdGameComponent.Initialize(this, ApplicationId);
            Components.Add(AdGameComponent.Current);

            advertising.AdRotatorInit();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // TODO: use this.Content to load your game content here

            if (graphics.PreferredBackBufferWidth < 600)
                generalFont = Content.Load<SpriteFont>(@"Fonts/general");
            else if (graphics.PreferredBackBufferWidth >= 600)
                generalFont = Content.Load<SpriteFont>(@"Fonts/general_big");
            else if (graphics.PreferredBackBufferWidth >= 800)
                generalFont = Content.Load<SpriteFont>(@"Fonts/general_biggest");

            tileTex = new Texture2D(GraphicsDevice, 1, 1);
            tileTex.SetData<Color>(new Color[] { Color.Red });

            filledTileTex = new Texture2D(GraphicsDevice, 1, 1);
            tileTex.SetData<Color>(new Color[] { Color.White });

            emptyTileTex = new Texture2D(GraphicsDevice, 1, 1);
            emptyTileTex.SetData<Color>(new Color[] { new Color(30, 30, 30) });

            boardTex = new Texture2D(GraphicsDevice, 1, 1);
            boardTex.SetData<Color>(new Color[] { Color.Black });

            shapeTrajectoryTex = new Texture2D(GraphicsDevice, 1, 1);
            shapeTrajectoryTex.SetData<Color>(new Color[] { new Color(84, 84, 84) });

            gameOverTex = Content.Load<Texture2D>(@"Images/game_over");
        }


        public void SaveGame()
        {
            if (GameInProgress)
                GamePersistance.Save(new PlayingGameState(gameSettings, gameBoard, shapesQueue, tetrisLogic.CurrentShape));
        }

        /// <summary>
        /// Loads a saved game and
        /// </summary>
        /// <returns>true - if saved game successfully loaded</returns>
        public bool LoadFromSave()
        {
            shapeGenerator = new ShapeGenerator();
            var state = GamePersistance.Load();
            if (state == null)
            {
                //if we can't read the saved game at least start a new one
                return false;
            }

            gameBoard = state.GameBoard;
            gameSettings = state.GameSettings;
            shapesQueue = state.ShapesQueue;

            tetrisLogic = new TetrisLogic(gameBoard);
            tetrisLogic.CurrentShape = state.CurrentShape;

            shapesQueue.Enqueue(shapeGenerator.GetRandomShape());
            return true;
        }


        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            switch (GameState)
            {
                case GameState.Playing: this.UpdateWhenPlaying(gameTime); break;
                case GameState.NewlyStarted: this.UpdateNewlyStarted(gameTime); break;
                case GameState.GameOver: this.UpdateGameOver(gameTime); break;
            }
            base.Update(gameTime);
        }

        protected void UpdateNewlyStarted(GameTime gameTime)
        {
            gameBoard = new GameBoard(20, 10);

            shapeGenerator = new ShapeGenerator();
            shapesQueue = new Queue<Shape>(SHAPES_IN_QUEUE);
            for (int i = 0; i < SHAPES_IN_QUEUE; i++)
            {
                shapesQueue.Enqueue(shapeGenerator.GetRandomShape());
            }

            tetrisLogic = new TetrisLogic(gameBoard);
            tetrisLogic.PlaceShapeFromQueue(shapesQueue);

            shapesQueue.Enqueue(shapeGenerator.GetRandomShape());

            gameSettings.Reset();

            if (GameMenu.NewGameSettings.Progressive)
                gameSettings.SetProgressive();
            else
                gameSettings.SetPlayLevel(GameMenu.NewGameSettings.Level);


            GameInProgress = true;
            GameState = GameState.Playing;
            this.UpdateWhenPlaying(gameTime);
        }

        protected void UpdateWhenPlaying(GameTime gameTime)
        {
            buttonRight.Update(gameTime);
            buttonLeft.Update(gameTime);
            buttonDown.Update(gameTime);
            buttonRotate.Update(gameTime);

            CheckBackPressOnUpdate(gameTime);

            // TODO: Add your update logic here
            var milSec = gameTime.ElapsedGameTime.Milliseconds;

            sinceLastFall += milSec;
            //TODO: Windows centric logic
            var mouse = Mouse.GetState();
            var cursor = new Point(mouse.X, mouse.Y);

            if (buttonRotate.MouseJustPressed)
            {
                tetrisLogic.TryRotate();
            }


            if (buttonRight.MouseJustPressed)
            {
                tetrisLogic.TryMoveHorizontally(1);
                timeSiceRightPressed = 0;
            }
            else if (buttonRight.MouseHeldMilliseconds > 0)
            {
                timeSiceRightPressed += milSec;
                if (timeSiceRightPressed >= 400)
                {
                    timeSiceRightPressed -= 67;
                    tetrisLogic.TryMoveHorizontally(1);
                }
            }

            if (buttonLeft.MouseJustPressed)
            {
                tetrisLogic.TryMoveHorizontally(-1);
                timeSinceLeftPressed = 0;
            }
            else if (buttonLeft.MouseHeldMilliseconds > 0)
            {
                timeSinceLeftPressed += milSec;
                if (timeSinceLeftPressed >= 400)
                {
                    timeSinceLeftPressed -= 67;
                    tetrisLogic.TryMoveHorizontally(-1);
                }
            }

            if (sinceLastFall >= gameSettings.FallTime || (buttonDown.MousePressed && sinceLastFall >= gameSettings.FastFallTime))
            {
                //TODO check if fell all over to bottom
                if (tetrisLogic.Collides(offsetY: 1))
                {
                    //fill board with new figure
                    tetrisLogic.FillBoardWithShape();

                    var addLines = gameBoard.ClearFilledRows();
                    gameSettings.UpdateFromFallenShape(addLines);

                    if (gameBoard.IsEndGame())
                    {
                        tetrisLogic.Collides(offsetY: 0);
                        GameState = GameState.GameOver;
                        GamePersistance.Delete();
                        this.GameMenu.SaveGameExists = false;
                        GameInProgress = false;
                    }
                    else
                    {
                        tetrisLogic.PlaceShapeFromQueue(shapesQueue);
                        shapesQueue.Enqueue(shapeGenerator.GetRandomShape());
                    }
                }
                else
                    tetrisLogic.CurrentShape.Y++;

                sinceLastFall = 0;
            }
        }


        protected void UpdateGameOver(GameTime gameTime)
        {
            CheckBackPressOnUpdate(gameTime);
        }


        private void CheckBackPressOnUpdate(GameTime gameTime)
        {
            // Goes to main menu
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed
                //TODO: windows centric
                || Keyboard.GetState(0).IsKeyDown(Keys.Escape)
                )
            {
                PressedBackFromGameInProgress = true;
                GameState = GameState.StartMenu;
                advertising.Show();

                base.Update(gameTime);
                return;
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            switch (GameState)
            {
                case GameState.Playing: DrawWhenPlaying(gameTime); break;
                case GameState.GameOver:
                    {
                        DrawWhenPlaying(gameTime);//draw the board anyway, we want game over to appear as a bow on top of board
                        DrawGameOver(gameTime);
                        break;
                    }
            }

            base.Draw(gameTime);
        }

        protected void DrawWhenPlaying(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            spriteBatch.Draw(boardTex, boardRect, Color.Black);
            spriteBatch.Draw(boardTex, shapeQueueRect, Color.Black);

            var linesPos = 10;
            spriteBatch.DrawString(generalFont, "LINES: \n" + gameSettings.LinesCleared, new Vector2(5, linesPos), Color.White);
            var scorePos = linesPos + 2 * TILE_DIM + 10;
            spriteBatch.DrawString(generalFont, "SCORE: \n" + gameSettings.GameScore, new Vector2(5, scorePos), Color.White);
            var levelPos = scorePos + 2 * TILE_DIM + 10;
            spriteBatch.DrawString(generalFont, "LEVEL: \n" + gameSettings.Level, new Vector2(5, levelPos), Color.White);


            // Draw the shapes queue
            for (int i = 0; i < SHAPES_IN_QUEUE; i++)
            {
                var shape = shapesQueue.ElementAt(i);

                Utils.ForEachInGrid(shape.Tiles, (x, y) =>
                {
                    if (shape.Tiles[y, x])
                    {
                        int xOffset = (x + 2) * TILE_DIM / 2 + shapeQueueRect.X;
                        int yOffset = (y + i * 5 + 1) * TILE_DIM / 2 + shapeQueueRect.Y;
                        spriteBatch.Draw(tileTex, new Rectangle(xOffset, yOffset, tileRect.Width / 2, tileRect.Height / 2), Color.Red);
                    }
                });
            }

            //Draw the filled board
            Utils.ForEachInGrid(gameBoard.Tiles, (x, y) =>
            {
                int xOffset = x * TILE_DIM + TILE_DIM / 10;
                int yOffset = y * TILE_DIM + TILE_DIM / 10;
                var texture = gameBoard.Tiles[y, x] ? tileTex : emptyTileTex;

                spriteBatch.Draw(texture, new Rectangle(boardRect.X + xOffset, boardRect.Y + yOffset, tileRect.Width, tileRect.Height), Color.White);
            });

            if (GameState == GameState.Playing)
            {
                var currentShape = tetrisLogic.CurrentShape;
                var set = currentShape.Tiles;
                var fallHeight = tetrisLogic.CalculateFallHeight();
                //Draw the current shape
                Utils.ForEachInGrid(set, (x, y) =>
                {
                    if (set[y, x])
                    {
                        int xOffset = (currentShape.X + x) * TILE_DIM + boardRect.X + TILE_DIM / 10;
                        int yOffset = (currentShape.Y + y) * TILE_DIM + boardRect.Y + TILE_DIM / 10;
                        int yOffsetWithFall = yOffset + TILE_DIM * fallHeight;
                        spriteBatch.Draw(shapeTrajectoryTex, new Rectangle(xOffset, yOffsetWithFall, tileRect.Width, tileRect.Height), Color.White);
                        spriteBatch.Draw(tileTex, new Rectangle(xOffset, yOffset, tileRect.Width, tileRect.Height), Color.Red);
                    }
                });
            }
            spriteBatch.End();
        }

        public void DrawGameOver(GameTime gameTime)
        {
            spriteBatch.Begin();

            spriteBatch.Draw(gameOverTex,
                position: new Vector2(boardRect.X, boardRect.Y + boardRect.Height / 2 - gameOverTex.Height / 2),
                sourceRectangle: null,
                color: Color.White,
                rotation: 0f,
                origin: Vector2.Zero,
                scale: new Vector2(boardRect.Width / (float)gameOverTex.Width),
                effects: SpriteEffects.None,
                layerDepth: 0f);

            spriteBatch.End();
        }

        /// <summary>
        /// Clean up the GeoCoordinateWatcher
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                if (this.gcw != null)
                {
                    this.gcw.Dispose();
                    this.gcw = null;
                }
            }
        }

    }
}
