using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace TetrisWindowsPhone
{
    public class PlayingGameState
    {
        public GameSettings GameSettings { get; set; }
        public GameBoard GameBoard { get; set; }
        public Queue<Shape> ShapesQueue { get; set; }
        public Shape CurrentShape { get; set; }

        public PlayingGameState(GameSettings gameSettings, GameBoard gameBoard, Queue<Shape> shapesQueue, Shape currentShape)
        {
            GameSettings = gameSettings;
            GameBoard = gameBoard;
            ShapesQueue = shapesQueue;
            CurrentShape = currentShape;
        }
    }


    public static class GamePersistance
    {
        private const string GAME_SAVE = "gamesave.sav";
        private const int SAVE_GAME_VERSION = 1;


        public static void Delete()
        {
            try
            {
                // Save the game state (in this case, the high score).
#if WINDOWS_PHONE
                IsolatedStorageFile savegameStorage = IsolatedStorageFile.GetUserStoreForApplication();
#else
                IsolatedStorageFile savegameStorage = IsolatedStorageFile.GetUserStoreForDomain();
#endif
                savegameStorage.DeleteFile(GAME_SAVE);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't delete game save");
            }
        }

        public static bool SaveGameExists()
        {
            try
            {
                // Save the game state (in this case, the high score).
#if WINDOWS_PHONE
                IsolatedStorageFile savegameStorage = IsolatedStorageFile.GetUserStoreForApplication();
#else
                IsolatedStorageFile savegameStorage = IsolatedStorageFile.GetUserStoreForDomain();
#endif
                return savegameStorage.FileExists(GAME_SAVE);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void Save(PlayingGameState playingGameState)
        {
            var gameSettings = playingGameState.GameSettings;
            var gameBoard = playingGameState.GameBoard;
            var shapesQueue = playingGameState.ShapesQueue;
            var currentShape = playingGameState.CurrentShape;
            try
            {
                // Save the game state (in this case, the high score).
#if WINDOWS_PHONE
                IsolatedStorageFile savegameStorage = IsolatedStorageFile.GetUserStoreForApplication();
#else
                IsolatedStorageFile savegameStorage = IsolatedStorageFile.GetUserStoreForDomain();
#endif

                // open isolated storage, and write the savefile.
                IsolatedStorageFileStream fs = null;
                using (fs = savegameStorage.CreateFile(GAME_SAVE))
                using (BinaryWriter writer = new BinaryWriter(fs))
                {
                    WriteSaveGameVersion(writer);
                    WriteGameSettings(gameSettings, writer);
                    WriteGameBoard(gameBoard, writer);
                    WriteShapesQueue(shapesQueue, writer);
                    WriteCurrentShape(currentShape, writer);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Couldn't save game");
            }
        }


        public static PlayingGameState Load()
        {
            GameSettings gameSettings;
            GameBoard gameBoard;
            Queue<Shape> shapesQueue;
            Shape currentShape;

            try
            {
                // open isolated storage, and load data from the savefile if it exists.
                #if WINDOWS_PHONE
                using (IsolatedStorageFile savegameStorage = IsolatedStorageFile.GetUserStoreForApplication())
                #else
                using (IsolatedStorageFile savegameStorage = IsolatedStorageFile.GetUserStoreForDomain())
                #endif
                {
                    if (savegameStorage.FileExists(GAME_SAVE))
                    {
                        using (IsolatedStorageFileStream fs = savegameStorage.OpenFile(GAME_SAVE, System.IO.FileMode.Open))
                        using (BinaryReader reader = new BinaryReader(fs))
                        {
                            if (SAVE_GAME_VERSION != ReadSaveGameVersion(reader))
                                throw new InvalidOperationException("Save game is from a previous version");
                            gameSettings = ReadGameSettings(reader);
                            gameBoard = ReadGameBoard(reader);
                            shapesQueue = ReadShapesQueue(reader);
                            currentShape = ReadCurrentShape(reader);
                        }
                    }
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return new PlayingGameState(gameSettings, gameBoard, shapesQueue, currentShape);
        }

        static int ReadSaveGameVersion(BinaryReader reader)
        {
            return reader.ReadInt32();
        }

        static void WriteSaveGameVersion(BinaryWriter writer)
        {
            writer.Write(SAVE_GAME_VERSION);
        }

        static GameSettings ReadGameSettings(BinaryReader reader)
        {
            GameSettings gameSettings = new GameSettings();

            var prog = reader.ReadBoolean();
            if (prog)
                gameSettings.SetProgressive();
            gameSettings.SetPlayLevelDoNotChangeProgressive(reader.ReadInt32());
            gameSettings.LinesCleared = reader.ReadInt32();
            gameSettings.GameScore = reader.ReadInt32();
            gameSettings.LinesSinceLevelUp = reader.ReadInt32();

            return gameSettings;
        }

        static void WriteGameSettings(GameSettings gameSettings, BinaryWriter writer)
        {
            writer.Write(gameSettings.IsProgressive);
            writer.Write(gameSettings.Level);
            writer.Write(gameSettings.LinesCleared);
            writer.Write(gameSettings.GameScore);
            writer.Write(gameSettings.LinesSinceLevelUp);
            writer.Flush();
        }

        static GameBoard ReadGameBoard(BinaryReader reader)
        {
            var rows = reader.ReadInt32();
            var cols = reader.ReadInt32();

            var gameBoard = new GameBoard(rows, cols);

            for (int y = 0; y < rows; y++)
                for (int x = 0; x < cols; x++)
                {
                    gameBoard.Tiles[y, x] = reader.ReadBoolean();
                }

            return gameBoard;
        }

        static void WriteGameBoard(GameBoard gameBoard, BinaryWriter writer)
        {
            int rows = gameBoard.Tiles.GetLength(0);
            int cols = gameBoard.Tiles.GetLength(1);
            writer.Write(rows);
            writer.Write(cols);

            for (int y = 0; y < rows; y++)
                for (int x = 0; x < cols; x++)
                {
                    writer.Write(gameBoard.Tiles[y,x]);
                }
        }

        static Queue<Shape> ReadShapesQueue(BinaryReader reader)
        {
            var count = reader.ReadInt32();

            Queue<Shape> queue = new Queue<Shape>(count);

            for (int i = 0; i < count; i++)
            {
                var shape = ShapeGenerator.CreateShapeByIndex(reader.ReadInt32());
                queue.Enqueue(shape);
            }

            return queue;
        }

        static void WriteShapesQueue(Queue<Shape> queue, BinaryWriter writer)
        {
            writer.Write(queue.Count);

            foreach (var shape in queue.AsEnumerable())
            {
                writer.Write(ShapeGenerator.GetShapeIndex(shape));
            }
        }

        static Shape ReadCurrentShape(BinaryReader reader)
        {
            int shapeIndex = reader.ReadInt32();
            int rotationIndex = reader.ReadInt32();
            int X = reader.ReadInt32();
            int Y = reader.ReadInt32();

            var shape = ShapeGenerator.CreateShapeByIndex(shapeIndex);
            shape.X = X;
            shape.Y = Y;

            for (int i = 0; i < rotationIndex; i++)
                shape.Rotate();

            return shape;
        }

        static void WriteCurrentShape(Shape shape, BinaryWriter writer)
        {
            writer.Write(ShapeGenerator.GetShapeIndex(shape));
            writer.Write(shape.GetRotationIndex());
            writer.Write(shape.X);
            writer.Write(shape.Y);
        }
    }
}
