﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TetrisWindowsPhone
{
    public abstract class Shape
    {
        protected bool[][,] tilePositionSets;
        protected int currentSetIndex;
        public Shape()
        {
            currentSetIndex = 0;
        }

        public int X { get; set; }
        public int Y { get; set; }

        public void Rotate()
        {
            Utils.DivRem(currentSetIndex + 1, tilePositionSets.Length, out currentSetIndex);
        }

        public void RotateBack()
        {
            if (currentSetIndex == 0)
                Utils.DivRem(tilePositionSets.Length - 1, tilePositionSets.Length, out currentSetIndex);
            else
                Utils.DivRem(currentSetIndex - 1, tilePositionSets.Length, out currentSetIndex);
        }

        public bool[,] Tiles
        {
            get
            {
                return tilePositionSets[currentSetIndex];
            }
        }

        public int GetRotationIndex()
        {
            return currentSetIndex;
        }
    }


    public class Square : Shape
    {
        public Square()
        {
            tilePositionSets = new bool[1][,];
            tilePositionSets[0] = new bool[2, 2] {{true, true}, {true, true}};
            currentSetIndex = 0;
        }
    }

    public class Stick : Shape
    {
        public Stick()
        {
            tilePositionSets = new bool[2][,];

            tilePositionSets[0] = new bool[4, 4] { { false, true, false, false }, { false, true, false, false }, { false, true, false, false }, { false, true, false, false } };
            tilePositionSets[1] = new bool[4, 4] { { false, false, false, false }, { true, true, true, true }, { false, false, false, false }, { false, false, false, false } };
            currentSetIndex = 0;
        }
    }

    public class TShape : Shape
    {
        public TShape()
        {
            tilePositionSets = new bool[4][,];

            tilePositionSets[0] = new bool[3, 3] { { false, true, false }, { false, true, true }, { false, true, false } };
            tilePositionSets[1] = new bool[3, 3] { { false, false, false }, { true, true, true }, { false, true, false } };
            tilePositionSets[2] = new bool[3, 3] { { false, true, false }, { true, true, false }, { false, true, false } };
            tilePositionSets[3] = new bool[3, 3] { { false, true, false }, { true, true, true }, { false, false, false } };
            currentSetIndex = 0;
        }
    }

    public class ZShape : Shape
    {
        public ZShape()
        {
            tilePositionSets = new bool[2][,];

            tilePositionSets[0] = new bool[3, 3] { { true, false, false }, { true, true, false }, { false, true, false } };
            tilePositionSets[1] = new bool[3, 3] { { false, false, false }, { false, true, true }, { true, true, false } };
        }
    }

    public class MirroredZShape : Shape
    {
        public MirroredZShape()
        {
            tilePositionSets = new bool[2][,];

            tilePositionSets[0] = new bool[3, 3] { { false, false, true }, { false, true, true }, { false, true, false } };
            tilePositionSets[1] = new bool[3, 3] { { false, false, false }, { true, true, false }, { false, true, true } };
        }
    }

    public class LShape : Shape
    {
        public LShape()
        {
            tilePositionSets = new bool[4][,];

            tilePositionSets[0] = new bool[3, 3] { { false, true, false }, { false, true, false }, { false, true, true } };
            tilePositionSets[1] = new bool[3, 3] { { false, false, false }, { true, true, true }, { true, false, false } };
            tilePositionSets[2] = new bool[3, 3] { { true, true, false }, { false, true, false }, { false, true, false } };
            tilePositionSets[3] = new bool[3, 3] { { false, false, true }, { true, true, true }, { false, false, false } };
        }
    }

    public class MirroredLShape : Shape
    {
        public MirroredLShape()
        {
            tilePositionSets = new bool[4][,];

            tilePositionSets[0] = new bool[3, 3] { { false, true, false }, { false, true, false }, { true, true, false } };
            tilePositionSets[1] = new bool[3, 3] { { true, false, false }, { true, true, true }, { false, false, false } };
            tilePositionSets[2] = new bool[3, 3] { { false, true, true }, { false, true, false }, { false, true, false } };
            tilePositionSets[3] = new bool[3, 3] { { false, false, false }, { true, true, true }, { false, false, true } };
        }
    }
}
