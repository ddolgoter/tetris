﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TetrisWindowsPhone
{
    /// <summary>
    /// Checks if various states between the Tetris board and and shapes are valid, also updates
    /// a given board and shape
    /// </summary>
    public class TetrisLogic
    {
        GameBoard _gameBoard;

        public TetrisLogic(GameBoard gameBoard)
        {
            _gameBoard = gameBoard;
        }

        public Shape CurrentShape { set; get; }

        public void FillBoardWithShape()
        {
            var set = CurrentShape.Tiles;
            int xLen = set.GetLength(0), yLen = set.GetLength(1);
            for (int y = 0; y < yLen; y++)
            {
                for (int x = 0; x < xLen; x++)
                {
                    if (set[y, x] && (y + CurrentShape.Y) >= 0)
                    {
                        _gameBoard.Tiles[y + CurrentShape.Y, x + CurrentShape.X] = true;
                    }
                }
            }
        }

        public void PlaceShapeFromQueue(Queue<Shape> queue)
        {
            var newShape = queue.Dequeue();
            newShape.Y = -2;

            var shapeSize = newShape.Tiles.GetLength(0);

            newShape.X = _gameBoard.Tiles.GetLength(1) / 2 - shapeSize / 2;
            CurrentShape = newShape;
        }

        public bool TryMoveHorizontally(int xChange)
        {
            if (!Collides(offsetX: xChange))
            {
                CurrentShape.X += xChange;
                return true;
            }
            return false;
        }

        public bool TryRotate()
        {
            CurrentShape.Rotate();
            if (Collides())
            {
                CurrentShape.RotateBack();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Calculates distance between the current position and when the shape hits the board
        /// </summary>
        public int CalculateFallHeight()
        {
            int y = -1;

            while (!Collides(offsetY: ++y)) ;

            return y - 1;
        }

        public bool Collides(int offsetX = 0, int offsetY = 0)
        {
            int newX = CurrentShape.X + offsetX, newY = CurrentShape.Y + offsetY;

            var tileSet = CurrentShape.Tiles;

            for (int y = 0; y < tileSet.GetLength(0); y++)
            {
                for (int x = 0; x < tileSet.GetLength(1); x++)
                {
                    if (tileSet[y, x] == false) continue;

                    //check if collides with left, right, bottom margins of the board
                    int tileOffsetX = newX + x;
                    if (tileOffsetX < 0)
                        return true;

                    if (tileOffsetX >= _gameBoard.Tiles.GetLength(1))
                        return true;

                    int tileOffsetY = newY + y;
                    if (tileOffsetY >= _gameBoard.Tiles.GetLength(0))
                        return true;

                    //now check if collides with filled tiles
                    if (tileOffsetX < 0 || tileOffsetY < 0)
                        continue;

                    if (_gameBoard.Tiles[tileOffsetY, tileOffsetX])
                        return true;
                }
            }

            return false;
        }

    }
}
