using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TetrisWindowsPhone
{
    public class Utils
    {
        public static int DivRem(int dividend, int divisor, out int remainder)
        {
            int quotient = dividend / divisor;
            remainder = dividend - divisor * quotient;
            return quotient;
        }

        public static bool PointWithinRectangle(Point point, Rectangle rectangle)
        {
            return point.X >= rectangle.X &&
                point.Y >= rectangle.Y &&
                point.X <= rectangle.X + rectangle.Width &&
                point.Y <= rectangle.Y + rectangle.Height;
        }

        public static bool RectangleClicked(Rectangle rectangle, MouseState mouseState)
        {
            return mouseState.LeftButton == ButtonState.Pressed && PointWithinRectangle(new Point(mouseState.X, mouseState.Y), rectangle);
        }

        /// <summary>
        /// Executes an action for each element in a boolean bidimensional grid
        /// </summary>
        /// <param name="grid">it's element will be foreached</param>
        /// <param name="action">has x and y positions as parameters</param>
        public static void ForEachInGrid(bool[,] grid, Action<int, int> action)
        {
            //Draw the filled board
            for (int y = 0; y < grid.GetLength(0); y++)
            {
                for (int x = 0; x < grid.GetLength(1); x++)
                {
                    action(x, y);
                }
            }
        }
    }
}
