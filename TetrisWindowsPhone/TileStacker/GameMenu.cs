﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Phone.Tasks;

namespace TetrisWindowsPhone
{
    /// <summary>
    /// Component for the game menu
    /// </summary>
    public class GameMenu : DrawableGameComponent
    {
        SpriteBatch _spriteBatch;
        Game1 _game;
        GameMenuState _gameMenuState;

        SpriteFont _gameMenuFont;
        Texture2D _gameMenuTex, _tileTex, _filledTileTex, _gameTitleText;
        GameMenuPixelData _menuPixelData;
        int _menuTexLeftOffset, _menuTexTopOffset, _pixelDim, _pixelPadding = 1;
        bool _pressedBackFromGameSettings = false;
        bool _settingsPressed = false;

        RectangularButton[] _levelButtons = new RectangularButton[15];
        RectangularButton _progressiveGameButton;
        RectangularButton _soundYesButton, _soundNoButton;
        Song _soundtrack;
        bool _mediaPlayerActiveOnStart;

        const int DisplayRedLetterMilliseconds = 2000;
        int _redDisplayTimeOut = 0;

        public NewGameSettings NewGameSettings { get; set; }
        public Rectangle NewGameBounds { get; set; }
        public Rectangle ContinueGameBounds { get; set; }
        public Rectangle SettingsBounds { get; set; }
        public Rectangle RateAppBounds { get; set; }

        public bool SaveGameExists = false;

        public GameMenu(Game1 game)
            : base(game)
        {
            _game = game;
            _gameMenuState = GameMenuState.General;
            NewGameSettings = new NewGameSettings();
            NewGameSettings.SetProgressive();
        }

        public override void Initialize()
        {
            NewGameSettings.Load();

            base.Initialize();
        }

        public void SetSizesForResolution()
        {
            _menuTexLeftOffset = (int)(_game.Graphics.PreferredBackBufferWidth * 0.083);
            _menuTexTopOffset = (int)(_game.Graphics.PreferredBackBufferHeight * 0.35);
            _pixelDim = (_game.Graphics.PreferredBackBufferWidth - 2 * _menuTexLeftOffset) / 40;

            if (_game.Graphics.PreferredBackBufferWidth >= 768)
                _pixelPadding = 2;

            var leftOffset = _menuTexLeftOffset + 3 * _pixelDim;
            var topOffset = _menuTexTopOffset + 5 * _pixelDim;

            NewGameBounds = new Rectangle(leftOffset, topOffset, 35 * _pixelDim, 5 * _pixelDim);
            ContinueGameBounds = new Rectangle(leftOffset, topOffset + 10 * _pixelDim, 35 * _pixelDim, 5 * _pixelDim);
            SettingsBounds = new Rectangle(leftOffset, topOffset + 20 * _pixelDim, 35 * _pixelDim, 5 * _pixelDim);
            RateAppBounds = new Rectangle(leftOffset, topOffset + 30 * _pixelDim, 35 * _pixelDim, 5 * _pixelDim);

            var width = _game.Graphics.PreferredBackBufferWidth;
            leftOffset = (int)(width * 0.03125);

            //Initialize game state buttons
            var progSize = _gameMenuFont.MeasureString("progressive");
            _progressiveGameButton = new RectangularButton("progressive",
                new Rectangle((int)(width * 0.08), (int)(width * 0.12), (int)(progSize.X + width * 0.06), (int)(width * 0.12)), _game, _game.GraphicsDevice);

            for (int i = 0; i < 15; i++)
            {
                var remainder = 0;
                Utils.DivRem(i, 5, out remainder);
                int y = i / 5;

                _levelButtons[i] = new RectangularButton((i + 1).ToString(),
                    new Rectangle(
                        (int)(width * 0.08 + (width * 0.17) * remainder),
                        ((int)(width * 0.44) + y * (int)(width * 0.17)),
                        (int)(width * 0.12),
                        (int)(width * 0.12)),
                        _game, _game.GraphicsDevice);
            }

            _soundYesButton = new RectangularButton("yes", new Rectangle((int)(width * 0.08), (int)(width * 1.1), (int)(width * 0.2), (int)(width * 0.12)), _game, _game.GraphicsDevice);
            _soundNoButton = new RectangularButton("no", new Rectangle((int)(width * 0.31), (int)(width * 1.1), (int)(width * 0.2), (int)(width * 0.12)), _game, _game.GraphicsDevice);
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(_game.GraphicsDevice);
            _gameMenuFont = Game.Content.Load<SpriteFont>(@"Fonts/gameMenu");
            _gameMenuTex = Game.Content.Load<Texture2D>(@"Images/game_menu");
            _gameTitleText = Game.Content.Load<Texture2D>(@"Images/game_title");
            _menuPixelData = new GameMenuPixelData(_gameMenuTex);

            _filledTileTex = new Texture2D(GraphicsDevice, 1, 1);
            _filledTileTex.SetData<Color>(new Color[] { Color.White });

            _tileTex = new Texture2D(GraphicsDevice, 1, 1);
            _tileTex.SetData<Color>(new Color[] { new Color(30, 30, 30) });

            if (MediaPlayer.State == MediaState.Stopped)
            {
                _mediaPlayerActiveOnStart = false;
                MediaPlayer.IsRepeating = true;
                _soundtrack = Game.Content.Load<Song>(@"Audio/Shifting-Sands");
                MediaPlayer.Play(_soundtrack);

                if (!NewGameSettings.Sound)
                {
                    MediaPlayer.Pause();
                }
            }
            else
            {
                _mediaPlayerActiveOnStart = true;
            }

            SetSizesForResolution();

            SaveGameExists = GamePersistance.SaveGameExists();

            base.LoadContent();
        }

        public void LoadAndPlaySoundrack()
        {
        }


        public override void Update(GameTime gameTime)
        {
            if (_game.GameState != GameState.StartMenu)
            {
                base.Update(gameTime);
                return;
            }

            switch (_gameMenuState)
            {
                case GameMenuState.General: UpdateGeneral(gameTime); break;
                case GameMenuState.Settings: UpdateSettings(gameTime); break;
            }


            base.Update(gameTime);
        }

        public void UpdateGeneral(GameTime gameTime)
        {
            var mouse = Mouse.GetState();

            if (Utils.RectangleClicked(NewGameBounds, mouse))
            {
                _game.GameState = GameState.NewlyStarted;
                _game.advertising.Hide();
            }

            if (Utils.RectangleClicked(ContinueGameBounds, mouse))
            {
                if (_game.GameInProgress || (SaveGameExists && _game.LoadFromSave()))
                {
                    _game.GameState = GameState.Playing;
                    _game.GameInProgress = true;
                    _game.advertising.Hide();
                }
            }

            if (Utils.RectangleClicked(SettingsBounds, mouse))
            {
                if (_gameMenuState == GameMenuState.General)
                {
                    _settingsPressed = true;
                    _gameMenuState = GameMenuState.Settings;
                }
            }

            
            if (Utils.RectangleClicked(RateAppBounds, mouse))
            {
                MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();

                marketplaceReviewTask.Show();
            }

            if (GamePad.GetState(0).Buttons.Back == ButtonState.Pressed || Keyboard.GetState(0).IsKeyDown(Keys.Escape))
            {
                if (_game.PressedBackFromGameInProgress == false && _pressedBackFromGameSettings == false)
                {
                    NewGameSettings.Save();
                    _game.SaveGame();
                    _game.Exit();
                }
            }

            if ((_game.PressedBackFromGameInProgress || _pressedBackFromGameSettings) &&
                (GamePad.GetState(0).Buttons.Back == ButtonState.Released && Keyboard.GetState(0).IsKeyUp(Keys.Escape)))
            {
                _game.PressedBackFromGameInProgress = false;
                _pressedBackFromGameSettings = false;
            }
        }

        public void UpdateSettings(GameTime gameTime)
        {
            var mouse = Mouse.GetState();

            //setting was pressed in a previous frame and not yet released
            if (_settingsPressed && mouse.LeftButton == ButtonState.Pressed)
                return;
            else
                _settingsPressed = false;

            if (GamePad.GetState(0).Buttons.Back == ButtonState.Pressed || Keyboard.GetState(0).IsKeyDown(Keys.Escape))
            {
                if (_gameMenuState == GameMenuState.Settings)
                {
                    _gameMenuState = GameMenuState.General;
                    _pressedBackFromGameSettings = true;
                }
            }

            if (Utils.RectangleClicked(_progressiveGameButton.Bounds, mouse))
            {
                NewGameSettings.SetProgressive();
            }

            for (int i = 0; i < _levelButtons.Length; i++)
            {
                if (Utils.RectangleClicked(_levelButtons[i].Bounds, mouse))
                    NewGameSettings.SetLevel(i + 1);
            }

            if (!_mediaPlayerActiveOnStart)
            {
                if (Utils.RectangleClicked(_soundYesButton.Bounds, mouse))
                {
                    MediaPlayer.Resume();
                    NewGameSettings.Sound = true;
                }

                if (Utils.RectangleClicked(_soundNoButton.Bounds, mouse))
                {
                    MediaPlayer.Pause();
                    NewGameSettings.Sound = false;
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            switch (_gameMenuState)
            {
                case GameMenuState.General: DrawGeneral(gameTime); break;
                case GameMenuState.Settings: DrawSettings(gameTime); break;
            }

            base.Draw(gameTime);
        }

        public void DrawGeneral(GameTime gameTime)
        {
            if (_game.GameState != GameState.StartMenu)
            {
                base.Draw(gameTime);
                return;
            }

            _spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

            var width = _game.Graphics.PreferredBackBufferWidth;
            _spriteBatch.Draw(_gameTitleText, new Rectangle(0, 0, width, width / 2), Color.White);

            _redDisplayTimeOut += gameTime.ElapsedGameTime.Milliseconds;
            if (_redDisplayTimeOut > 3 * DisplayRedLetterMilliseconds)
                _redDisplayTimeOut = 0;

            var filledWidth = _pixelDim - 2 * _pixelPadding;
            var leftOffset = _pixelPadding + _menuTexLeftOffset;
            var topOffseet = _pixelPadding + _menuTexTopOffset;
            for (int y = 0; y < _menuPixelData.Height; y++)
                for (int x = 0; x < _menuPixelData.Width; x++)
                {
                    var color = Color.White;

                    if (y > 10 && y < 20 && !_game.GameInProgress && !SaveGameExists)
                        color = Color.Gray;

                    if (_menuPixelData.GetPixel(x, y) == 2 && _redDisplayTimeOut < DisplayRedLetterMilliseconds)
                        color = Color.Red;

                    if (_menuPixelData.GetPixel(x, y) == 1 || _menuPixelData.GetPixel(x, y) == 2)
                        _spriteBatch.Draw(texture: _filledTileTex,
                            destinationRectangle: new Rectangle(x * _pixelDim + leftOffset, y * _pixelDim + topOffseet, filledWidth, filledWidth),
                            color: color);
                }

            _spriteBatch.End();
        }

        public void DrawSettings(GameTime gameTime)
        {
            var width = _game.Graphics.PreferredBackBufferWidth;
            float leftOffset = width * 0.03125f;

            _spriteBatch.Begin();

            _spriteBatch.DrawString(_gameMenuFont, "GAME TYPE", new Vector2(leftOffset, 0), Color.White);

            _progressiveGameButton.DrawButton(_spriteBatch, drawActive: NewGameSettings.Progressive);
            _spriteBatch.DrawString(_gameMenuFont, "OR CHOOSE LEVEL:", new Vector2(leftOffset, width * 0.32f), Color.White);

            for (int i = 0; i <= 14; i++)
            {
                _levelButtons[i].DrawButton(_spriteBatch, i + 1 == NewGameSettings.Level);
            }

            if (!_mediaPlayerActiveOnStart)
            {
                _spriteBatch.DrawString(_gameMenuFont, "IN GAME MUSIC:", new Vector2(leftOffset, width * 0.98f), Color.White);

                _soundYesButton.DrawButton(_spriteBatch, drawActive: NewGameSettings.Sound);
                _soundNoButton.DrawButton(_spriteBatch, drawActive: NewGameSettings.Sound == false);
            }

            _spriteBatch.End();
        }

    }
}
