﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TetrisWindowsPhone
{
    public class GameBoard
    {
        /// <summary>
        /// shows the filled tiles on the board
        /// </summary>
        public bool[,] Tiles; 

        public GameBoard(int rows, int cols)
        {
            Tiles = new bool[rows, cols];
        }

        public int ClearFilledRows()
        {
            bool[,] newTiles = new bool[Tiles.GetLength(0), Tiles.GetLength(1)];

            int newY = Tiles.GetLength(0) - 1;
            int linesCleared = 0;
            for (int y = Tiles.GetLength(0) - 1; y >= 0; y--)
            {
                bool allFilled = true;
                for (int x = 0; x < Tiles.GetLength(1); x++)
                {
                    if (Tiles[y, x] == false)
                    {
                        allFilled = false;
                        continue;
                    }
                }

                if (!allFilled)
                {
                    for (int x = 0; x < Tiles.GetLength(1); x++)
                    {
                        newTiles[newY, x] = Tiles[y, x];
                    }
                    newY--;
                }
                else
                    linesCleared++;
            }

            Tiles = newTiles;

            return linesCleared;
        }

        public bool IsEndGame()
        {
            //at least one should be true, notice that this function is supposed to be cleared of filled rows before
            for (int x = 0; x < Tiles.GetLength(1); x++)
                if (Tiles[0, x]) return true;

            return false;
        }
    }
}
