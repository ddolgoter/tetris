using Microsoft.Advertising;
using Microsoft.Advertising.Mobile.Xna;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Threading;

namespace TetrisWindowsPhone
{
    public class Advertising
    {
        private Game1 _game;
        private GraphicsDeviceManager _graphics;

        public Advertising(Game1 game, GraphicsDeviceManager graphics)
        {
            _game = game;
            _graphics = graphics;
        }

        public  void AdRotatorInit()
        {

            // TODO: Add your initialization logic here
            // Create a banner ad for the game.
            int width = 480;
            int height = 80;

            var rec = new Rectangle(0, _graphics.PreferredBackBufferHeight - 80, width, height);
            _game.bannerAd = AdGameComponent.Current.CreateAd(Game1.AdUnitId, rec, true);

            _game.bannerAd.AdRefreshed += new EventHandler(bannerAd_AdRefreshed);
            _game.bannerAd.ErrorOccurred += AdControl_ErrorOccurred;

            // Set some visual properties (optional).
            //bannerAd.BorderEnabled = true; // default is true
            _game.bannerAd.BorderColor = Color.Black; // default is White
            //bannerAd.DropShadowEnabled = true; // default is true

            // Provide the location to the ad for better targeting (optional).
            // This is done by starting a GeoCoordinateWatcher and waiting for the location to be available.
            // The callback will set the location into the ad. 
            // Note: The location may not be available in time for the first ad request.
            AdGameComponent.Current.Enabled = false;
            _game.gcw = new GeoCoordinateWatcher();
            _game.gcw.PositionChanged += new EventHandler<GeoPositionChangedEventArgs<GeoCoordinate>>(gcw_PositionChanged);
            _game.gcw.StatusChanged += new EventHandler<GeoPositionStatusChangedEventArgs>(gcw_StatusChanged);
            _game.gcw.Start();

        }

        /// <summary>
        /// This is called whenever a new ad is received by the ad client.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bannerAd_AdRefreshed(object sender, EventArgs e)
        {
            Debug.WriteLine("Ad received successfully");
        }

        private void gcw_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            // Stop the GeoCoordinateWatcher now that we have the device location.
            _game.gcw.Stop();

            _game.bannerAd.LocationLatitude = e.Position.Location.Latitude;
            _game.bannerAd.LocationLongitude = e.Position.Location.Longitude;

            AdGameComponent.Current.Enabled = true;

            Debug.WriteLine("Device lat/long: " + e.Position.Location.Latitude + ", " + e.Position.Location.Longitude);
        }

        private void gcw_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            if (e.Status == GeoPositionStatus.Disabled || e.Status == GeoPositionStatus.NoData)
            {
                // in the case that location services are not enabled or there is no data
                // enable ads anyway
                AdGameComponent.Current.Enabled = true;
                Debug.WriteLine("GeoCoordinateWatcher Status :" + e.Status);
            }
        }

        private void AdControl_ErrorOccurred(object sender, AdErrorEventArgs args)
        {
            AdGameComponent.Current.Visible = false;
            AdGameComponent.Current.Enabled = false;
            Debug.WriteLine(args.Error.Message);
        }

        public void Hide()
        {
            AdGameComponent.Current.Visible = false;
            AdGameComponent.Current.Enabled = false;
        }

        public void Show()
        {
            AdGameComponent.Current.Visible = true;
            AdGameComponent.Current.Enabled = true;
        }
    }
}
