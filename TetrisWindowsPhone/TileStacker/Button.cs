using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TetrisWindowsPhone
{
    public class RectangularButton
    {
        Rectangle _bounds;
        Texture2D _borderTex, _chromeTex;
        SpriteFont _textFont;
        String _text;
        Vector2 _textOffset;


        public RectangularButton(String text, Rectangle bounds, Game game, GraphicsDevice graphicsDevice)
        {
            _text = text;
            _bounds = bounds;
            _textFont = game.Content.Load<SpriteFont>(@"Fonts/gameMenu");
            _borderTex = new Texture2D(graphicsDevice, 1, 1);
            _borderTex.SetData<Color>(new Color[] { Color.White });

            _chromeTex = new Texture2D(graphicsDevice, 1, 1);
            _chromeTex.SetData<Color>(new Color[] { Color.White });

            var textSize = _textFont.MeasureString(_text);
            _textOffset.X = (_bounds.Width - textSize.X) / 2;
            _textOffset.Y = (_bounds.Height - textSize.Y) / 2;
        }

        /// <summary>
        /// Assume begin and end where called by the caller of this method
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void DrawButton(SpriteBatch spriteBatch, bool drawActive = false)
        {
            var color = drawActive ? Color.Red : Color.White;

            spriteBatch.Draw(texture: _borderTex, destinationRectangle: new Rectangle(_bounds.X, _bounds.Y, 3, _bounds.Height), color: color);
            spriteBatch.Draw(texture: _borderTex, destinationRectangle: new Rectangle(_bounds.X, _bounds.Y, _bounds.Width, 3), color: color);
            spriteBatch.Draw(texture: _borderTex, destinationRectangle: new Rectangle(_bounds.X + _bounds.Width, _bounds.Y, 3, _bounds.Height), color: color);
            spriteBatch.Draw(texture: _borderTex, destinationRectangle: new Rectangle(_bounds.X, _bounds.Y + _bounds.Height, _bounds.Width + 3, 3), color: color);

            spriteBatch.DrawString(spriteFont: _textFont, text: _text, position: new Vector2(_bounds.X + _textOffset.X, _bounds.Y + _textOffset.Y), color: color);
        }

        public Rectangle Bounds
        {
            get
            {
                return _bounds;
            }
        }
    }
}
