﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.IO;
using System.Linq;
using System.Text;

namespace TetrisWindowsPhone
{
    public class NewGameSettings
    {
        public bool Progressive { get; private set; }
        public int Level { get; private set; }
        public bool Sound { get; set; }

        public NewGameSettings()
        {
            Sound = false;
        }

        public void SetProgressive()
        {
            Level = 0;
            Progressive = true;
        }

        public void SetLevel(int level)
        {
            Level = level;
            Progressive = false;
        }

        public void Save()
        {
            try
            {
                // Save the game state (in this case, the high score).
                #if WINDOWS_PHONE
                IsolatedStorageFile savegameStorage = IsolatedStorageFile.GetUserStoreForApplication();
                #else
                IsolatedStorageFile savegameStorage = IsolatedStorageFile.GetUserStoreForDomain();
                #endif

                // open isolated storage, and write the savefile.
                IsolatedStorageFileStream fs = null;
                using (fs = savegameStorage.CreateFile("state.sav"))
                using (BinaryWriter writer = new BinaryWriter(fs))
                {
                    writer.Write(Progressive);
                    writer.Write(Level);
                    writer.Write(Sound);
                    writer.Flush();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Coudn't save game settins");
            }
        }

        public void Load()
        {
            try
            {
                // open isolated storage, and load data from the savefile if it exists.
                #if WINDOWS_PHONE
                using (IsolatedStorageFile savegameStorage = IsolatedStorageFile.GetUserStoreForApplication())
                #else
                using (IsolatedStorageFile savegameStorage = IsolatedStorageFile.GetUserStoreForDomain())
                #endif
                {
                    if (savegameStorage.FileExists("state.sav"))
                    {
                        using (IsolatedStorageFileStream fs = savegameStorage.OpenFile("state.sav", System.IO.FileMode.Open))
                        using (BinaryReader reader = new BinaryReader(fs))
                        {
                            Progressive = reader.ReadBoolean();
                            Level = reader.ReadInt32();
                            Sound = reader.ReadBoolean();
                        }
                    }
                }
            }
            catch (Exception)
            {
                Progressive = true;
                Level = 0;
                Sound = true;
            }
        }
    }
}
