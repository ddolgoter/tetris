﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TetrisWindowsPhone
{
    public enum GameState 
    {
        StartMenu = 1,
        NewlyStarted = 2,
        Playing = 4,
        GameOver = 8
    }

    public enum GameMenuState
    {
        General = 1,
        Settings = 2
    }
}
