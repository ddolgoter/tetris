using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TetrisWindowsPhone
{
    public class ShapeGenerator
    {
        private Random _rand;

        public ShapeGenerator()
        {
            _rand = new Random();
        }

        public Shape GetRandomShape()
        {
            int idx = _rand.Next(7);

            return CreateShapeByIndex(idx);
        }

        public static Shape CreateShapeByIndex(int idx)
        {
            switch (idx)
            {
                case 0: return new Square();
                case 1: return new Stick();
                case 2: return new TShape();
                case 3: return new ZShape();
                case 4: return new MirroredZShape();
                case 5: return new LShape();
                case 6: return new MirroredLShape();
            }

            throw new InvalidOperationException("Trying to generate wrong shape");
        }

        public static int GetShapeIndex(Shape shape)
        {
            if (shape is Square) return 0;
            if (shape is Stick) return 1;
            if (shape is TShape) return 2;
            if (shape is ZShape) return 3;
            if (shape is MirroredZShape) return 4;
            if (shape is LShape) return 5;
            if (shape is MirroredLShape) return 6;

            throw new InvalidOperationException("Trying to generate wrong shape");
        }
    }
}
